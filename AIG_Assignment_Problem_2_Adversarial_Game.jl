### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ 28b6ff64-9311-4fd4-9cee-7cda8575dd80
using LinearAlgebra

# ╔═╡ 99b5dc5c-089c-4b24-929d-9e2c112ae54b
md"## Create an abstract game type for reference purposes"

# ╔═╡ 300e9440-adc0-11eb-1eea-e9928cefb76b
abstract type AbstractGame end

# ╔═╡ 9044f1b9-cf12-4e56-ae0b-5b1581f10067
md"### Initialize the states of the game"

# ╔═╡ 6ce8979b-be28-41b7-9ad3-b9d28f89b489
struct TicTacToeState
    turn::String
    utility::Int64
    board::Dict
    moves::AbstractVector
end
   

# ╔═╡ 1b6ec379-fd45-4036-af43-151e35bb442b
md"### Function for taking  turns, assigning utility value, and the making a move on the board"

# ╔═╡ 2efa426e-f22e-4265-85eb-d4d402c54f3f
 function TicTacToeStates(turn::String, utility::Int64, board::Dict, moves::AbstractVector)
        return new(turn, utility, board, moves)
    end

# ╔═╡ d7259471-b98a-43b9-9053-ebd7465d8330
md"### Function to perform full search for Alpha"

# ╔═╡ d81cadc3-ca79-4f48-9902-dafe77a2e71a


# ╔═╡ 228f3e05-df64-48ce-94e5-6111c2bf273f
md"### Function to perform full search for Alpha and Beta"

# ╔═╡ 3f1a2b37-652d-48f8-9f72-36a8dcf09785
function alphabeta_full_search(state::String, game::T) where {T <: AbstractGame}
	local player::String = to_move(game, state);
    return argmax(actions(game, state), 
                    (function(action::String,; relevant_game::AbstractGame=game, relevant_state::String=state, relevant_player::String=player)
                        return alphabeta_full_search_min_value(relevant_game, relevant_player, result(relevant_game, relevant_state, action), -Inf64, Inf64);
                    end))
end

# ╔═╡ 1e8abeeb-fd62-4e72-bc5d-dc7ceb154fac
md"### Function to perform search for Alpha value"

# ╔═╡ b75c1453-948c-4c46-a691-86a2f37b73b9
md"### Function to perform search for Beta Value"

# ╔═╡ e5753cf4-3696-42f6-b68b-ba62f2f1219a
md"### Function to draw board"

# ╔═╡ 7ddfdc65-ce0e-4882-9826-1d7bd9aeb362
function make_board(N)
    # creates an empty board of size N x N
    rows = [Char(x+48) for x=1:N]
    columns = [x for x='A':'Z'][1:N]
    return zeros(Int, N, N), rows, columns
end

# ╔═╡ f7fea980-6729-46fe-9c4f-32505866316e
md"### Function to make a move"

# ╔═╡ d3537c57-b390-4244-aeff-183251c02814
function move(board, player, position)
    b = copy(board)
    i, j = position
    if b[i,j] != 0
        :invalid_move
    else
        b[i,j] = player
        b
    end
end

# ╔═╡ 45e8f547-1758-4170-84fb-d5219f7c2404
md"### Function to define the state of the game"

# ╔═╡ 01a0cb48-db41-45ff-ac16-c226dff37827
function game_state(board)
    # returns :continue_game if the game is ongoing
    # 1 if player X has won
    # -1 if player O has won
    # 0 if there are no more moves
    N, _ = size(board)
    # rows and columns
    for i=1:N
        if sum(board[i,:]) == N || sum(board[:,i]) == N
            return 1
        elseif sum(board[i,:]) == -N || sum(board[:,i]) == -N
            return -1
        end
    end
    # diagonals
    if sum(diag(board,0)) == N || sum(diag(rotl90(board),0)) == N
        return 1
    elseif sum(diag(board,0)) == -N || sum(diag(rotl90(board),0)) == -N
        return -1
    elseif ~any(x -> x==0, board)
        return 0
    end
    # game continues
    return :continue_game
end


# ╔═╡ ff823559-d9ce-47be-9568-e94d9db9f108
md"### Function to mark string to the board"

# ╔═╡ a54a4434-36d5-439d-b728-9c1a42fda208
md"### Function to mark player's move"

# ╔═╡ d7744523-b69e-48ad-a01d-dd74b406c5c7
function player_to_mark(x)
    if x == -1
        out = "O"
    elseif x == 1
        out = "X"
    else
        out = " "
    end
    out
end

# ╔═╡ 68da698f-d89e-4ee8-b29b-5a0a79065c69
function board_to_string(board, rows, columns)
    function row_to_string(row, fun)
        local out = ""
        for i=row
            out = string(out, "\t", fun(i))
        end
        out
    end

    width, height = size(board)
    out = string("\t", row_to_string([x for x=columns], string), "\n")
    for j=1:height # use this rather than 'rows' as we need an integer index
        row_seq = [x for x=board[j,:]]
        row_string = row_to_string(row_seq, player_to_mark)
        out = string(out, string(j), "\t", row_string, "\n")
    end
    out
end


# ╔═╡ 8b53af1e-ffd8-49b5-af56-0d0d403a4a48
md"### Function to take in input from player"

# ╔═╡ 48e764e1-1cb1-4a05-8a01-d4c5947d8e71
function parse_input(input, rows, columns)
    if length(input) == 1
		col = findfirst(x -> x == input[1], columns)
        row = findfirst(x -> x == input[1], rows)
        if isdefined(Base, :col) && isdefined(Base, :row) && (col > 0) && (row > 0)
            return (row, col)
        else
            return :invalid_input
        end
    elseif input == "END"
        return :end
    else
        return :invalid_input
    end
end

# ╔═╡ 0b176348-7eb0-4593-b2dd-3734aef63654
md"### Function to get the status of player definition"

# ╔═╡ 651116eb-dd72-4a95-8b6d-7b9f4e0ada1e
function get_player_status(player)
    player_string = (player == 1) ? "X" : "O"
    println("Player $player_string: (P)erson or (A)I?")
		input = readline(stdin)
    if input == "P"
        :human
    elseif input == "A"
        :computer
    else
        println("Chose 'P' for Human or 'A' for AI...")
        get_player_status(player)
    end
end

# ╔═╡ ca23109d-0b9b-43f3-9482-92d9116f0a14
md"### Function to start the game"

# ╔═╡ 0530dcee-e219-4b94-9966-a3909efdce01
function main()
    # setup game and first player
    board, rows, columns = make_board(3)
    player = 1
    # get player status Human or Computer
    players = [1, -1]
    player_type = Dict()
    for p=players
        player_type[p] = get_player_status(p)
    end
    # main game loop
    while game_state(board) == :continue_game
        println(board_to_string(board, rows, columns))
        if player_type[player] == :human
            println("Enter your move:")
            input = readline(stdin)
            command = parse_input(input, rows, columns)
            if command == :end
                break
            elseif command == :invalid_input
                println("Invalid input")
            else
                new_board = move(board, player, command)
                if new_board == :invalid_move
                    println("Invalid move")
                else
                    board = new_board
                    player *= -1
                end
            end
        else
            ai_util, ai_move = alphabeta(board, player)
            board = move(board, player, ai_move)
            player *= -1
        end
    end
    if game_state(board) == 1
        println(board_to_string(board, rows, columns))
        println(string("PLAYER A (Xs) WINS!"))
    elseif game_state(board) == -1
        println(board_to_string(board, rows, columns))
        println(string("PLAYER B (Os) WINS!"))
    elseif game_state(board) == 0
        println(board_to_string(board, rows, columns))
        println("GAME OVER: NO MOVES AVAILABLE")
    else
        println("GAME STOPPED")
    end
end

# ╔═╡ 4b1f03ec-4804-4f5d-91f8-aea7c47f7522
main()

# ╔═╡ 0dced3bd-2afa-476f-a752-4bb162252e1b


# ╔═╡ 06aaaa44-b604-4c87-8b63-d67be65c6e81


# ╔═╡ 0dab50bc-6aa4-4b17-b625-d8cd8c8c4a9d


# ╔═╡ 716142c2-77d7-4cdd-b0cd-7792f4d03b32
function alphabeta_search_max_value(game::T, player::String, cutoff_test_fn::Function, evaluation_fn::Function, state::String, alpha::Number, beta::Number, depth::Int64) where {T <: AbstractGame}
    if (cutoff_test_fn(state, depth))
        return evaluation_fn(state)
    end
    local v::Float64 = -Inf64;
    for action in actions(game, state)
        v = max(v, alphabeta_search_min_value(game, player, cutoff_test_fn, evaluation_fn, result(game, state, action), alpha, beta, depth + 1));
        if (v >= beta)
            return v
        end
        alpha = max(alpha, v)
    end
    return v
end

# ╔═╡ 0b278105-cc14-4421-aed9-c1586950b130
function alphabeta_full_search_max_value(game::T, player::String, state::String, alpha::Number, beta::Number) where {T <: AbstractGame}
	if (terminal_test(game, state))
		return utility(game, state, player)
	end
	local v::Float64 = -Inf64
	for action in actions(game, state)
		v = max(v, alphabeta_full_search_min_value(game, player, result(game, state, action), alpha, beta))
        if (v >= beta)
            return v
        end
        alpha = max(alpha, v)
	end
	return v
end


# ╔═╡ 3973fa41-8a4b-4afb-aa73-988336b51296
function alphabeta_search_min_value(game::T, player::String, cutoff_test_fn::Function, evaluation_fn::Function, state::TicTacToeState, alpha::Number, beta::Number, depth::Int64) where {T <: AbstractGame}
    if (cutoff_test_fn(state, depth))
        return evaluation_fn(state)
    end
    local v::Float64 = Inf64
    for action in actions(game, state)
        v = min(v, alphabeta_search_max_value(game, player, cutoff_test_fn, evaluation_fn, result(game, state, action), alpha, beta, depth + 1));
        if (v >= alpha)
            return v
        end
        beta = min(alpha, v)
    end
    return v
end


# ╔═╡ b865c37b-d7e5-4cae-83b1-6cabdb43dd92
function alphabeta_full_search_min_value(game::T, player::String, state::String, alpha::Number, beta::Number) where {T <: AbstractGame}
    if (terminal_test(game, state))
        return utility(game, state, player)
    end
    local v::Float64 = Inf64
    for action in actions(game, state)
        v = min(v, alphabeta_full_search_max_value(game, player, result(game, state, action), alpha, beta))
        if (v <= alpha)
            return v
        end
        beta = min(beta, v)
    end
    return v
end

# ╔═╡ Cell order:
# ╠═99b5dc5c-089c-4b24-929d-9e2c112ae54b
# ╠═300e9440-adc0-11eb-1eea-e9928cefb76b
# ╠═9044f1b9-cf12-4e56-ae0b-5b1581f10067
# ╠═6ce8979b-be28-41b7-9ad3-b9d28f89b489
# ╠═1b6ec379-fd45-4036-af43-151e35bb442b
# ╠═2efa426e-f22e-4265-85eb-d4d402c54f3f
# ╠═d7259471-b98a-43b9-9053-ebd7465d8330
# ╠═0b278105-cc14-4421-aed9-c1586950b130
# ╠═d81cadc3-ca79-4f48-9902-dafe77a2e71a
# ╠═b865c37b-d7e5-4cae-83b1-6cabdb43dd92
# ╠═228f3e05-df64-48ce-94e5-6111c2bf273f
# ╠═3f1a2b37-652d-48f8-9f72-36a8dcf09785
# ╠═1e8abeeb-fd62-4e72-bc5d-dc7ceb154fac
# ╠═716142c2-77d7-4cdd-b0cd-7792f4d03b32
# ╠═b75c1453-948c-4c46-a691-86a2f37b73b9
# ╠═3973fa41-8a4b-4afb-aa73-988336b51296
# ╠═28b6ff64-9311-4fd4-9cee-7cda8575dd80
# ╠═e5753cf4-3696-42f6-b68b-ba62f2f1219a
# ╠═7ddfdc65-ce0e-4882-9826-1d7bd9aeb362
# ╠═f7fea980-6729-46fe-9c4f-32505866316e
# ╠═d3537c57-b390-4244-aeff-183251c02814
# ╠═45e8f547-1758-4170-84fb-d5219f7c2404
# ╠═01a0cb48-db41-45ff-ac16-c226dff37827
# ╠═ff823559-d9ce-47be-9568-e94d9db9f108
# ╠═68da698f-d89e-4ee8-b29b-5a0a79065c69
# ╠═a54a4434-36d5-439d-b728-9c1a42fda208
# ╠═d7744523-b69e-48ad-a01d-dd74b406c5c7
# ╠═8b53af1e-ffd8-49b5-af56-0d0d403a4a48
# ╠═48e764e1-1cb1-4a05-8a01-d4c5947d8e71
# ╠═0b176348-7eb0-4593-b2dd-3734aef63654
# ╠═651116eb-dd72-4a95-8b6d-7b9f4e0ada1e
# ╠═ca23109d-0b9b-43f3-9482-92d9116f0a14
# ╠═0530dcee-e219-4b94-9966-a3909efdce01
# ╠═4b1f03ec-4804-4f5d-91f8-aea7c47f7522
# ╠═0dced3bd-2afa-476f-a752-4bb162252e1b
# ╠═06aaaa44-b604-4c87-8b63-d67be65c6e81
# ╠═0dab50bc-6aa4-4b17-b625-d8cd8c8c4a9d
