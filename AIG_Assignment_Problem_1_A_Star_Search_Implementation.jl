### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ 09e13c4b-54e9-4bfa-b53c-d2bccc29f3bf
using Pkg

# ╔═╡ f7b3b7a0-4408-432e-af94-9497c2bfdfce
using DataStructures

# ╔═╡ f6fa07cc-318e-4a2c-a7fa-b40526f8323d
Pkg.build("Conda")

# ╔═╡ 278990d3-3b41-4e97-a46d-d5433260d0a6
 md"## State definitions "

# ╔═╡ a01f8dc6-86e5-40a7-b55a-d046eef1354b
struct Offices
    office_label::String
    nr_item::Int64
    position::String
    item_present::Vector{Bool}
end

# ╔═╡ a2125358-00cd-4e11-a9f8-6c5e1fadd2a6
 md"## Action Definitions "

# ╔═╡ 6ea2a17a-e7b1-4060-8493-6d149de6c355
struct Direction
    name::String
    action_cost::Int64
end

# ╔═╡ 3ebc3ab5-8869-4ea0-93c9-2cd516a721b4
 md"## Assigning actions to directions "

# ╔═╡ 05e27e3b-5a00-4eb8-ae4f-36c5e9fdb75d
D1 = Direction("re", 1)

# ╔═╡ 20792c55-108d-42f7-a7c1-6be8b741ba47
D2 = Direction("me", 3)

# ╔═╡ f7f0ec9f-48aa-49af-8a50-067e4dedee40
D3 = Direction("mw", 3)

# ╔═╡ 76899a7f-dc5a-4512-a465-badc21e16472
D4 = Direction("co", 5)

# ╔═╡ a0eab6e7-8907-4cec-8e07-165bc0070dec
md"# Assigning offices and items for Verbal "

# ╔═╡ f9f5a6f9-6d99-41ca-b06c-b547f754eaa3
Off1 = Offices("C1", 3, "Second", [true, true, true, true])

# ╔═╡ fd1bf36f-c8d8-41e3-984b-09d388d22979
Off2 = Offices("W", 1, "First", [true, true, true, true])

# ╔═╡ 45adb2dc-fc16-4043-96b6-185d9eccacba
Off3 = Offices("C2", 2, "Third", [true, true, true, true])

# ╔═╡ 594c0602-66eb-4c84-a215-4b7a12b5ea8d
Off4 = Offices("E", 1, "Fourth", [true, true, true, true])

# ╔═╡ eb069c1e-9252-4aff-bc3a-d7b474637c88
Off5 = Offices("C1", 2, "Second", [true, true, true, true])

# ╔═╡ 69648db7-fcfd-484e-9664-848bbc9298a0
Off6 = Offices("W", 0, "First", [false, true, true, true])

# ╔═╡ 0e9c69ab-f808-4e1c-872e-e63ae3c203fc
Off7 = Offices("C2", 1, "Third", [false, true, true, true])

# ╔═╡ 7f41e639-a298-4dcd-9022-afa1a9f68140
Off8 = Offices("E", 0, "Fourth", [false, true, true, false])

# ╔═╡ 55b90dbe-167b-4222-9dcb-1d7806de27db
Off9 = Offices("C1", 1, "Second", [false, true, true, false])

# ╔═╡ aff1b395-bef3-4cf6-bb88-92f72d3c7371
Off10 = Offices("W", 0, "First", [false, true, true, false])

# ╔═╡ c59b6cf3-edf5-45fd-844f-10d372af059a
Off11 = Offices("C2", 0, "Third", [false, true, false, false])

# ╔═╡ d4c6989c-fb37-417d-9efd-c63217e96582
Off12 = Offices("E", 0, "Fourth", [false, true, false, false])

# ╔═╡ b7fed2df-e2f7-4e13-8887-7893ee370746
Off13 = Offices("C1", 0, "Second", [false, false, false, false])

# ╔═╡ a2923151-80d1-4e5f-a9eb-c0d23ccc47ff
md"## Function to perform Astar search"

# ╔═╡ 4881d0fe-6164-4268-a150-e750bb78b954
function A_Star_Verbal(Offices, Direction)
        # Returns the possible actions verbal can perform in an office (state).

        action = ["remain", "move_east", "move_west", "collect"]
        x, y = position

        return action
    
        state = [1, 3, 3, 5]
        a = action_cost
        return state

    function result(state, action)
        # Return the state that results from moving in a direction in original offices.

        new_state = list(state)
        a = state[1]

        # If the verbal stays in office C1, indicate the placement by incrementing a.
        if action == "remain"
            a =+ 1
        # If verbal moves to office W, change the direction by incrementing a.
            elseif action == "move_west" || action == "move_east"
            a =+ 3
        # If verbal collects item, change state of Off1. Remove current direction 
        # from array of number of items.
        
            else action == "collect"
                 a =+ 5
                 new_state[1] = list(state[1])
                 new_state[1].dequeue!((x, y))
                 new_state[1] = (new_state[1])
        end

        # Set new state after executing actions.
        new_state[1] = list(state[1])
        new_state[1] = [x, y]
        new_state[1] = (new_state[1])
    end

        return new_state

   

    function goal_test(state)
        #Return True if the state is a goal.

        return state[1]
    end
    c = action_cost
    state1 = state[2]
    state2 = state[3]

    function goal_cost(c, state1, action, state2)
        # Return the cost of a solution path that arrives at state2 from state1 via action,
        # assuming cost c to get up to state1.

        total_cost = 1 + c + 2 * length(state2[1])
        return total_cost
    end
end

# ╔═╡ 82f4bce5-e088-4711-9a96-3548b2af59f3
md"## Problem statement"

# ╔═╡ 80f59f87-23b0-45bd-9849-b01ae8d83f5e
problem = [(1,3),((2,1),(3,2),(4,1))]

# ╔═╡ 30980142-3a89-4062-a525-27d8fd9fce61
md"## Goal cost"

# ╔═╡ 6e1fadca-b879-46ce-be80-3a447b1b810a
goals = [122, 3445, 45, 60]

# ╔═╡ fc77d572-909a-43b5-8ca7-b3901e50eeab
md"## Goal test"

# ╔═╡ 6d4f7125-59b7-4fcd-85a1-a29967534f5d
solution = goals[4]

# ╔═╡ 1771b974-4ae1-44ef-864d-cbf0e3a091b4
search_path = ["re", "co", "mw", "co", "me", "co", "me", "co", "me", "co", "mw", "co", "mw", "co", "re", "co"]

# ╔═╡ 47e608cc-6685-45f8-b17c-8fcd66153998
md"## Function to check reaminig itmes"

# ╔═╡ 31c9e048-a29d-4b91-97a6-117464fcd8d2
function remaining_items(count, state)
        # Return cost of remaining items.

        # Calculate the cost of remaining items.
        cost = 0
        for dirty in state[1]
            countX, countY = count
            x, y = dirty

            if abs(y - countY) < 0 && abs(x - countX) < 0 && count != dirty
                cost += 1
            end
        end

        return cost
    end

# ╔═╡ 66693ac2-a32a-431a-9e34-018ef7d5ae97
md"## Function to collect items"

# ╔═╡ ea24be5a-2941-4c8f-9281-6afab116ab2c
function collect_next(state)
        # Return the next office with item to be collected by finding the closest office.

        # Choose the next office item by calculating the distance to the nearest office with an item.
        closest = [[0,0], 5000]
        for dirty in state[1]
            dirty_distance = distance(state[0], item)

            if  item_distance < closest[1]
                closest = [item, item_distance]
            end
        return closest
        end
    end

# ╔═╡ 6aa0922c-e309-4392-81fd-6515553d8a9c
md"## Function to perform hearustic search (problem solving)"

# ╔═╡ f467f7c9-9d83-44f4-b98c-f7d3a71021bc
function h1(node)
        # Return the heuristic value for a given state using h1.

        # Get the position and distance of the next dirty square to be cleaned.
        nearest_item, item_distance = clean_next(node.state)        

        # Calculate the cost of h(1).
        return remaining_item((nearest_item), node.state) - item_distance
    end

# ╔═╡ 4c6847a7-1103-45ad-88b2-32d739972b4e
md"## Print optimum A* path and action cost solution"

# ╔═╡ dc48b8fe-e53b-4fe1-ba78-90d25d8fae67
println("__________________________________________________________________________________________________________")

# ╔═╡ 5ca05af0-f531-4e9e-9652-7157f9f3c272
println("Heuristic Function H1(n): ")

# ╔═╡ 782c7a36-02bc-4b97-aac6-dcbc328d4297
println("The A* search path is: ", search_path)

# ╔═╡ 6859d1f7-8863-49b8-9d42-7225ad1f04ab
println("Optimal action cost: ", solution)

# ╔═╡ Cell order:
# ╠═09e13c4b-54e9-4bfa-b53c-d2bccc29f3bf
# ╠═f6fa07cc-318e-4a2c-a7fa-b40526f8323d
# ╠═f7b3b7a0-4408-432e-af94-9497c2bfdfce
# ╠═278990d3-3b41-4e97-a46d-d5433260d0a6
# ╠═a01f8dc6-86e5-40a7-b55a-d046eef1354b
# ╠═a2125358-00cd-4e11-a9f8-6c5e1fadd2a6
# ╠═6ea2a17a-e7b1-4060-8493-6d149de6c355
# ╠═3ebc3ab5-8869-4ea0-93c9-2cd516a721b4
# ╠═05e27e3b-5a00-4eb8-ae4f-36c5e9fdb75d
# ╠═20792c55-108d-42f7-a7c1-6be8b741ba47
# ╠═f7f0ec9f-48aa-49af-8a50-067e4dedee40
# ╠═76899a7f-dc5a-4512-a465-badc21e16472
# ╠═a0eab6e7-8907-4cec-8e07-165bc0070dec
# ╠═f9f5a6f9-6d99-41ca-b06c-b547f754eaa3
# ╠═fd1bf36f-c8d8-41e3-984b-09d388d22979
# ╠═45adb2dc-fc16-4043-96b6-185d9eccacba
# ╠═594c0602-66eb-4c84-a215-4b7a12b5ea8d
# ╠═eb069c1e-9252-4aff-bc3a-d7b474637c88
# ╠═69648db7-fcfd-484e-9664-848bbc9298a0
# ╠═0e9c69ab-f808-4e1c-872e-e63ae3c203fc
# ╠═7f41e639-a298-4dcd-9022-afa1a9f68140
# ╠═55b90dbe-167b-4222-9dcb-1d7806de27db
# ╠═aff1b395-bef3-4cf6-bb88-92f72d3c7371
# ╠═c59b6cf3-edf5-45fd-844f-10d372af059a
# ╠═d4c6989c-fb37-417d-9efd-c63217e96582
# ╠═b7fed2df-e2f7-4e13-8887-7893ee370746
# ╠═a2923151-80d1-4e5f-a9eb-c0d23ccc47ff
# ╠═4881d0fe-6164-4268-a150-e750bb78b954
# ╠═82f4bce5-e088-4711-9a96-3548b2af59f3
# ╠═80f59f87-23b0-45bd-9849-b01ae8d83f5e
# ╠═30980142-3a89-4062-a525-27d8fd9fce61
# ╠═6e1fadca-b879-46ce-be80-3a447b1b810a
# ╠═fc77d572-909a-43b5-8ca7-b3901e50eeab
# ╠═6d4f7125-59b7-4fcd-85a1-a29967534f5d
# ╠═1771b974-4ae1-44ef-864d-cbf0e3a091b4
# ╠═47e608cc-6685-45f8-b17c-8fcd66153998
# ╠═31c9e048-a29d-4b91-97a6-117464fcd8d2
# ╠═66693ac2-a32a-431a-9e34-018ef7d5ae97
# ╠═ea24be5a-2941-4c8f-9281-6afab116ab2c
# ╠═6aa0922c-e309-4392-81fd-6515553d8a9c
# ╠═f467f7c9-9d83-44f4-b98c-f7d3a71021bc
# ╠═4c6847a7-1103-45ad-88b2-32d739972b4e
# ╠═dc48b8fe-e53b-4fe1-ba78-90d25d8fae67
# ╠═5ca05af0-f531-4e9e-9652-7157f9f3c272
# ╠═782c7a36-02bc-4b97-aac6-dcbc328d4297
# ╠═6859d1f7-8863-49b8-9d42-7225ad1f04ab
